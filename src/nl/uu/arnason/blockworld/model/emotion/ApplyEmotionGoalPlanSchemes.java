package nl.uu.arnason.blockworld.model.emotion;

import java.util.List;

import oo2apl.agent.DeliberationStepToAgentInterface;
import oo2apl.agent.Goal;
import oo2apl.agent.Trigger;
import oo2apl.defaults.deliberationsteps.DefaultDeliberationStep;
import oo2apl.deliberation.DeliberationStepException;
import oo2apl.plan.PlanScheme;

public class ApplyEmotionGoalPlanSchemes extends DefaultDeliberationStep {

	public ApplyEmotionGoalPlanSchemes(DeliberationStepToAgentInterface deliberationInterface) {
		super(deliberationInterface); 
	}
 
	public void execute() throws DeliberationStepException {
		// Clear the goals, achieved goals will be removed
		List<Goal> oldGoals = super.deliberationInterface.getGoals();
		super.deliberationInterface.clearAchievedGoals();
		List<Goal> newGoals = super.deliberationInterface.getGoals();
		
		// Old goals that are not present are achieved and hence set to joyful
		for(Goal goal : oldGoals){
			if(goal instanceof EGoal && !newGoals.contains(goal)){
				((EGoal)goal).setEmotion(EState.Emotion.JOY);
			}
		}
		
		List<? extends Trigger> triggers = super.deliberationInterface.getGoals();

		//@Sigurdur Arnason: apply the coping strategies
		applyCopingStrategies(triggers);

		super.applyTriggerInterceptors(triggers, super.deliberationInterface.getGoalInterceptors());
		List<PlanScheme> planSchemes = super.deliberationInterface.getGoalPlanSchemes();
		myApplyPlanSchemes(triggers, planSchemes);
	}
	
	private void myApplyPlanSchemes(final List<? extends Trigger> triggers, final List<PlanScheme> planSchemes){
		for(Trigger trigger : triggers){
			// For goals check whether there is not already a plan instantiated for the goal. In this implementation each goal can have
			// at most one instantiated plan scheme that tries to achieve that goal.
			if(!(trigger instanceof Goal && ((Goal)trigger).isPursued())){
				// @Sigurdur Arnason: if this is an EGoal and is not hopeFearful then we don't make a plan
				if(trigger instanceof EGoal && !((EGoal) trigger).getEState().isHopeFearful())
					continue;
				boolean foundPlan = false;
				for(PlanScheme planScheme : planSchemes){
					if(this.deliberationInterface.tryApplication(trigger, planScheme)){
						foundPlan = true;
						break;
					}
				}
				// @Sigurdur Arnason: if no plan was applicable for an EGoal it becomes distressed
				if(!foundPlan && trigger instanceof EGoal) {
					((EGoal) trigger).setEmotion(EState.Emotion.DISTRESS);
				}
			}
		}
	}
	
	private void applyCopingStrategies(List<? extends Trigger> triggers) {
		// The only case where no EGoal is hopeFearful is if there is no neutral EGoal to make hopeFearful
		boolean hopeFearfulEGoal = false; // false until we know that there is at least one EGoal that is hopeFearful
		boolean neutralEGoal = false; // false until we know that there is at least one EGoal that is neutral
		// if there is no active EGoal then we try to find a neutral EGoal and activate it by making it hopeFearful
		for(Trigger trigger : triggers) {
			if(trigger instanceof EGoal) {
				if(((EGoal) trigger).getEState().isHopeFearful())
					hopeFearfulEGoal = true;
				if(((EGoal) trigger).getEState().isNeutral())
					neutralEGoal = true;
			}
		}
		if(!hopeFearfulEGoal && neutralEGoal)
		for(Trigger trigger : triggers) {
			if(trigger instanceof EGoal) {
				if(((EGoal) trigger).getEState().isNeutral()) {
					((EGoal) trigger).setEmotion(EState.Emotion.HOPE_FEAR);
					break;
				}
			}
		}
	}
}
