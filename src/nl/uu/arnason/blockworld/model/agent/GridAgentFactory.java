package nl.uu.arnason.blockworld.model.agent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nl.uu.arnason.blockworld.model.agent.PlanSchemes.DestinationGoalPlanExecutionErrorPlanScheme;
import nl.uu.arnason.blockworld.model.agent.PlanSchemes.DestinationGoalPlanScheme;
import nl.uu.arnason.blockworld.model.agent.PlanSchemes.GoalUpdateTriggerPlanScheme;
import nl.uu.arnason.blockworld.model.agent.PlanSchemes.GridUpdateTriggerPlanScheme;
import nl.uu.arnason.blockworld.model.emotion.ApplyEmotionGoalPlanSchemes;
import oo2apl.agent.AgentComponentFactory;
import oo2apl.agent.AgentType;
import oo2apl.agent.Context;
import oo2apl.agent.ContextArguments;
import oo2apl.agent.ContextContainer;
import oo2apl.agent.DeliberationStepToAgentInterface;
import oo2apl.defaults.deliberationsteps.ApplyExternalTriggerPlanSchemes; 
import oo2apl.defaults.deliberationsteps.ApplyInternalTriggerPlanSchemes;
import oo2apl.defaults.deliberationsteps.ApplyMessagePlanSchemes;
import oo2apl.defaults.deliberationsteps.ExecutePlans;
import oo2apl.deliberation.DeliberationStep; 
import oo2apl.plan.PlanScheme;
import oo2apl.plan.PlanSchemeBase;
import oo2apl.plan.PlanSchemeBaseArguments;

public class GridAgentFactory implements AgentComponentFactory{
	private Context model;
	private Context goalBase;
	private Context sensor;
	private Context actuator;
	
	public GridAgentFactory(BeliefBase model, GoalBase goalBase, Sensor sensor, Actuator actuator){
		this.model = model;
		this.goalBase = goalBase;
		this.sensor = sensor;
		this.actuator = actuator;
	}
	
	public AgentType getAgentType() { 
		return GridAgentType.getInstance();
	}
 
	public ContextContainer produceContextContainer(ContextArguments contextArgs) {
		ContextContainer container = new ContextContainer();
		container.addContext(this.model);
		container.addContext(this.goalBase);
		container.addContext(this.sensor);
		container.addContext(this.actuator); 
		return container;
	}
 
	public PlanSchemeBase producePlanSchemeBase(PlanSchemeBaseArguments planSchemeBaseArgs) {
		List<PlanScheme> internalSchemes = new ArrayList<>();
		internalSchemes.add(new DestinationGoalPlanExecutionErrorPlanScheme());
		List<PlanScheme> externalSchemes = new ArrayList<>();
		externalSchemes.add(new GridUpdateTriggerPlanScheme());
		externalSchemes.add(new GoalUpdateTriggerPlanScheme());
		List<PlanScheme> goalSchemes = new ArrayList<>();
		goalSchemes.add(new DestinationGoalPlanScheme());
		return new PlanSchemeBase(goalSchemes, internalSchemes, externalSchemes, Collections.emptyList());
	} 
	
	public List<DeliberationStep> produceDeliberationCycle(final DeliberationStepToAgentInterface deliberationInterface){
		// Produces the adapted emotional 2APL deliberation cycle.
		List<DeliberationStep> deliberationCycle = new ArrayList<>();
		deliberationCycle.add(new ApplyEmotionGoalPlanSchemes(deliberationInterface));
		deliberationCycle.add(new ApplyExternalTriggerPlanSchemes(deliberationInterface));
		deliberationCycle.add(new ApplyInternalTriggerPlanSchemes(deliberationInterface));
		deliberationCycle.add(new ApplyMessagePlanSchemes(deliberationInterface));
		deliberationCycle.add(new ExecutePlans(deliberationInterface));
		return deliberationCycle;
	} 
}
