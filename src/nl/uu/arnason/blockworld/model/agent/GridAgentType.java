package nl.uu.arnason.blockworld.model.agent;

import oo2apl.agent.AgentType; 

public class GridAgentType implements AgentType {
	private static final AgentType instance = new GridAgentType();
	public static final AgentType getInstance(){ return instance; }
}
